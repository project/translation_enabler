<?php

function translation_enabler_settings(&$form_state) {

  // this form could potentially be a column in locale_languages_overview_form
  $languages = language_list('enabled');
  $languages = $languages[1];

  $form['help'] = array(
     '#description' => t('Is the language you want missing?  Go add it in your locale settings.'),
     '#type' => 'item',
  );
  $form['heading'] = array(
     '#title' => t('Enabled Languages'),
     '#description' => t('Uncheck modules that are not ready'),
     '#type' => 'item',
  );
  foreach($languages as $code => $value) {
    $form['translation_enabler_lang_' . $code] = array(
       '#type' => 'checkbox', 
       '#title' => $value->native,
       '#default_value' => variable_get('translation_enabler_lang_'. $code , FALSE),
    );
  }
  return system_settings_form($form);
}
